/*
 * input_driver.h
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#ifndef INC_INPUT_DRIVER_H_
#define INC_INPUT_DRIVER_H_

_Bool getKeyState();
_Bool getLimitSwitchState();
_Bool getFastSlowState();
_Bool getUpButtonState();
_Bool getDownButtonState();
_Bool getInputState(int inputNumber);


#endif /* INC_INPUT_DRIVER_H_ */
