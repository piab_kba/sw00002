/*
 * encoder_driver.h
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#ifndef INC_ENCODER_DRIVER_H_
#define INC_ENCODER_DRIVER_H_

void initEncoder();
int getEncoderPosition();
void setEncoder(int value);
void Encoder();

#endif /* INC_ENCODER_DRIVER_H_ */
