/*
 * eeprom_driver.h
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#ifndef INC_EEPROM_DRIVER_H_
#define INC_EEPROM_DRIVER_H_

void initEeprom();
int getStoredPosition(int Position);
int getTotalStroke();
void setTotalStroke(_Bool setBit);
void storePosition(int Position);
void totalStrokeCalibrateHandler();
void positionCalibrateHandler(int Position);

#endif /* INC_EEPROM_DRIVER_H_ */
