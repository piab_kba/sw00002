/*
 * PID.h
 *
 *  Created on: 24 sep. 2021
 *      Author: johcar
 */

#ifndef PID_H_
#define PID_H_

typedef struct {

	/* Controller gains */
	float Kp; 				/* 10.0 */
	float Ki;				/* 0.0015 */
	float Kd;				/* 0.5 */

	/* Derivative low-pass filter time constant */
	float tau;

	/* Output limits */
	float limMin;
	float limMax;

	/* Sample time (in seconds) */
	float T;

	/* Controller "memory" */
	float integrator;
	float prevError;			/* Required for integrator */
	float differentiator;
	float prevMeasurement;		/* Required for differentiator */

	/* Controller output */
	float out;

} PIDController;

void InitPIDController(PIDController *pid);
float UpdatePIDController(PIDController *pid, float setpoint, float measurement);

#endif /* PID_H_ */
