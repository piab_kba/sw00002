/*
 * motion_driver.c
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */


#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"
#include "dac_driver.h"
#include "PID.h"

#define DAC_RESOLUTION    (8) // Set this value to 9, 8, 7, 6 or 5 to adjust the resolution
#define setDirUp PORTB |= (1 << PB0);
#define setDirDown PORTB &= ~(1 << PB0);

/* Controller parameters */
#define PID_KP  2.0f
#define PID_KI  0.5f
#define PID_KD  0.25f

#define PID_TAU 0.02f

#define PID_LIM_MIN -10.0f
#define PID_LIM_MAX  10.0f

#define PID_LIM_MIN_INT -5.0f
#define PID_LIM_MAX_INT  5.0f

#define SAMPLE_TIME_S 0.01f

int slowZoneTop = 800;
int slowZoneBottom = 600;
int compensationTop = 50;
int compensationBottom = 50;
int breakInProcessBottom = 500;

PIDController pid = {
		PID_KP, PID_KI, PID_KD, PID_TAU,
		PID_LIM_MIN, PID_LIM_MAX,PID_LIM_MIN_INT,
		PID_LIM_MAX_INT, SAMPLE_TIME_S };

void moveUp(){
	setDirUp;
}

void moveDown(){
	setDirDown;
}

void moveStop(){
	WriteDevMCP4725(0, false);
}

void moveFast(){
	WriteDevMCP4725(4095, false);
}

void moveSlow(){
	WriteDevMCP4725(2000, false);
}

void initMotionSystem(){

	DDRB = 0x10;
	DDRC = 0x00;
	DDRD = 0x00;
	ADMUX = 0x3C; //0b01100000
	ADCSRA = 0x87; //0b10000111

	InitDevMCP4725();

	InitPIDController(&pid);

	moveStop();
}

void breakInProcess(){
	for (int i = 0; i < 10; i++) {
		for (int i = 0; i < 3; i++) {
			ledBlink(100);
		}
	}
	calibrateEncoder();
	for (int full = 0; full < 15; full++) {
		while (getEncoderPosition() < getTotalStroke()) {
			getEncoderPosition();
			getTotalStroke();
			moveUp();
		}
		while (getEncoderPosition() > breakInProcessBottom) {
			getEncoderPosition();
			moveDown();
		}
		while (getEncoderPosition() < (getTotalStroke() / 2)) {
			getEncoderPosition();
			getTotalStroke();
			moveUp();
		}
		while (getEncoderPosition() > breakInProcessBottom) {
			getEncoderPosition();
			moveDown();
		}
		moveStop();
		ledBreakInProcessDone();
	}
}

void calibrateEncoder(){
	if (getLimitSwitchState() == true) {
		moveUp();
		moveFast();
		_delay_ms(3000);
		moveStop();
	}
	while (getLimitSwitchState() != true) {
		moveFast();
		moveDown();
		getLimitSwitchState();
	}
	setEncoder(0);
	ledOn();
	moveStop();
	_delay_ms(500);
	ledOff();
}

void positionMove(int buttonNumber){
	while (getInputState(buttonNumber) != true){
		if (getInputState(buttonNumber) == true){ ledOff(); moveStop(); return; }
		if (UpdatePIDController(&pid, getStoredPosition(buttonNumber) ,getEncoderPosition()) < 0){
			moveUp();
		}
		else if(UpdatePIDController(&pid, getStoredPosition(buttonNumber) ,getEncoderPosition()) > 0){
			moveDown();
		}
		WriteDevMCP4725(UpdatePIDController(&pid, getStoredPosition(buttonNumber), getEncoderPosition()),false);
	}
	moveStop();
}

void downMove(){
	while (getDownButtonState() != true){
		if (getDownButtonState() == true) { moveStop(); return; }
		if (UpdatePIDController(&pid, compensationBottom, getEncoderPosition()) < 0){
			moveUp();
		}
		else if(UpdatePIDController(&pid, compensationBottom, getEncoderPosition()) > 0){
			moveDown();
		}
		if (getFastSlowState() == true){
			WriteDevMCP4725(UpdatePIDController(&pid, compensationBottom, getEncoderPosition()),false);
		}
		else{
			if (UpdatePIDController(&pid, compensationBottom, getEncoderPosition()) > 3000){
				WriteDevMCP4725(UpdatePIDController(&pid, compensationBottom, getEncoderPosition())/2,false);
			}
			else{
				WriteDevMCP4725(UpdatePIDController(&pid, compensationBottom, getEncoderPosition()),false);
			}
		}
	}
	moveStop();
}

void upMove(){
	while (getUpButtonState() != true) {
		if (getUpButtonState() == true) { moveStop(); return; }
		if (UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition()) < 0){
			moveUp();
		}
		else if(UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition()) > 0){
			moveDown();
		}
		if (getFastSlowState() == true){
			WriteDevMCP4725(UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition()),false);
		}
		else{
			if (UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition()) > 3000){
				WriteDevMCP4725(UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition())/2,false);
			}
			else{
				WriteDevMCP4725(UpdatePIDController(&pid, getTotalStroke(), getEncoderPosition()),false);
			}
		}
	}
	moveStop();
}
