/*
 * input_driver.c
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */


#include <avr/io.h>
#include <stdbool.h>
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

#define KSwitch PINB & (1 << PINB4);
#define LSwitch PIND & (1 << PIND7);
#define FSwitch PIND & (1 << PIND4);
#define R1 PINC & (1 << PINC0);
#define R2 PINC & (1 << PINC1);
#define C1 PINC & (1 << PINC2);
#define C2 PINC & (1 << PINC3);
#define C3 PINB & (1 << PINB1);
#define C4 PINB & (1 << PINB2);
#define C5 PINB & (1 << PINB3);

int threshold = 800;

_Bool getKeyState(){
	_Bool returnState;
	returnState = KSwitch;
	return returnState;
}

_Bool getLimitSwitchState(){
	_Bool returnState;
	returnState = LSwitch;
	return returnState;
}

_Bool getFastSlowState(){
	_Bool returnState;
	returnState = FSwitch;
	return returnState;
}

_Bool getUpButtonState(){
	_Bool returnState;
	ADMUX |= (1 << 2 | 1 << 3); // will produce 0bxxxxx110 in turn activating ADC6 as ADC pin.
	ADCSRA = ADCSRA | (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
	if (ADCH > threshold){
		returnState = true;
	}
	else
	{
		returnState = false;
	}
	return returnState;
}

_Bool getDownButtonState(){
	_Bool returnState;
	ADMUX |= (1 << 1 | 1 << 2 | 1 << 3); // will produce 0bxxxxx111 in turn activating ADC7 as ADC pin.
    	ADCSRA = ADCSRA | (1 << ADSC);
    	while (ADCSRA & (1 << ADSC));
    	if (ADCH > threshold){
    		returnState = true;
    	}
    	else
    	{
    		returnState = false;
    	}
    	return returnState;
}

_Bool getInputState(int inputNumber){
	_Bool returnState;
	_Bool readRow;
	_Bool readColumn;
	switch (inputNumber) {
    	case 1: readRow = R1; readColumn = C1; break;
    	case 2: readRow = R2; readColumn = C1; break;
    	case 3: readRow = R1; readColumn = C2; break;
    	case 4: readRow = R2; readColumn = C2; break;
    	case 5: readRow = R1; readColumn = C3; break;
    	case 6: readRow = R2; readColumn = C3; break;
    	case 7: readRow = R1; readColumn = C4; break;
    	case 8: readRow = R2; readColumn = C4; break;
    	case 9: readRow = R1; readColumn = C5; break;
    	case 10: readRow = R2; readColumn = C5; break;
	}
	returnState = readRow & readColumn;
	return returnState;
}
