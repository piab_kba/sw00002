/*
 * encoder_driver.c
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */


#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

#define EncoderA PIND & (1 << PIND2);
#define EncoderB PIND & (1 << PIND3);

_Bool phaseA = false;
_Bool phaseB = false;
_Bool lastA = false;
_Bool lastB = false;

unsigned int Encoder0Pos;

void initEncoder(){

	EICRA |= (1 << ISC00); //Configure INT0 to sensing for "change" on Encoder phase A pin.

	EIMSK |= (1 << INT0); //Enables Interrupt Request for INT0.

	sei();  // Enable global interrupts by setting global interrupt enable bit in SREG.

}

int getEncoderPosition(){
	return Encoder0Pos;
}

void setEncoder(int value){
	Encoder0Pos = value;
}

ISR(PCINT0_vect){
	phaseA = EncoderA;
	phaseB = EncoderB;
	if (phaseA != lastA){
		if (phaseA == true){
			if (phaseB != true){
				Encoder0Pos--;
			}
			else{
				Encoder0Pos++;
			}
		}
	}
	lastA = phaseA;
	lastB = phaseB;
}
