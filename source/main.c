/*
 * main.c
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */


#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <stdbool.h>
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

_Bool powerOut = true;
int threshold = 800;

int main(void) {

	initEeprom();

	initMotionSystem();

	initEncoder();

	while(1){
		while (powerOut == true) {
			while (getKeyState() != true) {
				ledOff();
				if (getDownButtonState() != true && getInputState(1) != true) breakInProcess();
				while (getInputState(1) != true) {
					ledOn();
					if (getKeyState() == true){
						setTotalStroke(false);
						ledRestoreStroke();
					}
				}
			}
			ledOn();
			if (getInputState(1) != true && getInputState(2) != true) {
				ledOff();
				calibrateEncoder();
				powerOut = false;
			}
		}
		while (getKeyState() != true) {
			if (getUpButtonState() != true && getInputState(1) != true) totalStrokeCalibrateHandler();
			for (int i = 1; i <= 10; i++){
				if (getInputState(i) != true) positionCalibrateHandler(i);
			}
		}
		for (int i = 1; i <= 10; i++){
			if (getInputState(i) != true) positionMove(i);
			if (getUpButtonState() != true) upMove();
			if (getDownButtonState() != true) downMove();
		}
	}
}
