/*
 * dac_driver.c
 *
 *  Created on: 20 sep. 2021
 *      Author: johcar
 */


#include "dac_driver.h"
#include <stdint.h>
#include <math.h>
#include <stdbool.h>

#define I2C_READ    1
#define I2C_WRITE   0
#define MCP4725_INIT_OUTPUT			20	///< Desired initial output value
#define MCP4725_DEVICE_ID 			0x62 	///< Default i2c address
#define MCP4725_CMD_WRITEDAC 		0x40	///< Writes data to the DAC
#define MCP4725_CMD_WRITEDACEEPROM 	0x60 	///< Writes data to the DAC and the EEPROM (persisting the assigned
         	 	 	 	 	 	 	 	 	///< value after reset

void InitDevMCP4725(){
	int dec = GetDevMCP4725EEPROM();
	if (dec != MCP4725_INIT_OUTPUT) SetDevMCP4725EEPROM();
}

_Bool WriteDevMCP4725(uint16_t output, _Bool writeEEPROM/*, uint32_t i2c_frequency*/){
	uint8_t senpack[3];
	if (writeEEPROM){
		senpack[0] = MCP4725_CMD_WRITEDACEEPROM;
	}
	else{
		senpack[0] = MCP4725_CMD_WRITEDAC;
	}
	senpack[1] = output / 16;
	senpack[2] = (output % 16) << 4;
	i2c_start_wait(I2C_WRITE);
	i2c_write(senpack[0]);
	i2c_write(senpack[1]);
	i2c_write(senpack[2]);
	i2c_stop();
	return true;
}

int GetDevMCP4725EEPROM(){
	uint8_t retpack[3];
	i2c_init();
	i2c_rep_start(MCP4725_DEVICE_ID+I2C_READ);
	retpack[1] = i2c_readAck();
	retpack[2] = i2c_readAck();
	retpack[3] = i2c_readAck();
	i2c_stop();
	uint16_t combine = (retpack[3] << 8)| retpack[2];
	long long n = combine;
	int dec = 0, i = 0, rem;
	while(n != 0){
		rem = n % 16;
		n /= 10;
		dec += rem * pow(2, i);
		i++;
	}
	return(dec);
}

_Bool SetDevMCP4725EEPROM(){
	uint8_t init_output[3];
	init_output[0] = MCP4725_CMD_WRITEDACEEPROM;
	init_output[1] = MCP4725_INIT_OUTPUT / 16;
	init_output[2] = (MCP4725_INIT_OUTPUT % 16) << 4;
	i2c_start_wait(MCP4725_DEVICE_ID+I2C_WRITE);
	i2c_write(init_output[0]);
	i2c_write(init_output[1]);
	i2c_write(init_output[2]);
	i2c_stop();
	return true;
}
