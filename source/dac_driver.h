/*
 * dac_driver.h
 *
 *  Created on: 20 sep. 2021
 *      Author: johcar
 */

#ifndef DAC_DRIVER_H_
#define DAC_DRIVER_H_

#if (__GNUC__ * 100 + __GNUC_MINOR__) < 304
#error "This library requires AVR-GCC 3.4 or later, update to newer AVR-GCC compiler !"
#endif

#include <avr/io.h>

extern void i2c_init(void);

extern void i2c_stop(void);

extern unsigned char i2c_start(unsigned char addr);

extern unsigned char i2c_rep_start(unsigned char addr);

extern void i2c_start_wait(unsigned char addr);

extern unsigned char i2c_write(unsigned char data);

extern unsigned char i2c_readAck(void);

extern unsigned char i2c_readNak(void);

extern unsigned char i2c_read(unsigned char ack);
#define i2c_read(ack)  (ack) ? i2c_readAck() : i2c_readNak();

void InitDevMCP4725();

_Bool WriteDevMCP4725(uint16_t output, _Bool writeEEPROM/*, uint32_t i2c_frequency*/);

int GetDevMCP4725EEPROM();

_Bool SetDevMCP4725EEPROM();

#endif /* DAC_DRIVER_H_ */
