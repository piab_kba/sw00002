/*
 * indicator_driver.h
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#ifndef INC_INDICATOR_DRIVER_H_
#define INC_INDICATOR_DRIVER_H_

void ledOn();
void ledOff();
void ledBlink(int blinkPace);
void ledRestoreStroke();
void ledBreakInProcessDone();

#endif /* INC_INDICATOR_DRIVER_H_ */
