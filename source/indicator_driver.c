/*
 * indicator_driver.c
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

#define LedOn PORTB |= (1 << PB5);
#define LedOff PORTB &= ~(1 << PB5);

void ledOn(){
	LedOn;
}

void ledOff(){
	LedOff;
}

void ledBlink(int blinkPace){
	ledOn();
	_delay_ms(blinkPace);
	ledOff();
	_delay_ms(blinkPace);
}

void ledRestoreStroke(){
	for (int i = 0; i < 20; i++){
		ledBlink(50);
	}
}

void ledBreakInProcessDone(){
	_Bool getInput = true;
	while (getInput){
		for (int i = 1; i < 10; i++){
			if (getInputState(i) != true) break;
			if (getUpButtonState() != true) break;
			if (getDownButtonState() != true) break;
			ledBlink(1000);
		}
	}
}
