################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../PID.c \
../dac_driver.c \
../eeprom_driver.c \
../encoder_driver.c \
../indicator_driver.c \
../input_driver.c \
../main.c \
../motion_driver.c 

OBJS += \
./PID.o \
./dac_driver.o \
./eeprom_driver.o \
./encoder_driver.o \
./indicator_driver.o \
./input_driver.o \
./main.o \
./motion_driver.o 

C_DEPS += \
./PID.d \
./dac_driver.d \
./eeprom_driver.d \
./encoder_driver.d \
./indicator_driver.d \
./input_driver.d \
./main.d \
./motion_driver.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -O1 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=20000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


