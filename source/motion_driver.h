/*
 * motion_driver.h
 *
 *  Created on: 17 sep. 2021
 *      Author: johcar
 */

#ifndef INC_MOTION_DRIVER_H_
#define INC_MOTION_DRIVER_H_

void moveUp();
void moveDown();
void moveStop();
void moveFast();
void moveSlow();
void initMotionSystem();
void breakInProcess();
void calibrateEncoder();
void positionMove(int buttonNumber);
void upMove();
void downMove();

#endif /* INC_MOTION_DRIVER_H_ */
